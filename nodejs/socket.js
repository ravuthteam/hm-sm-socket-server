require('dotenv').config();
let app = require('express')();
let fs = require('fs');

let options = {};
if (process.env.APP_ENV === 'production') {
    options = {
        key: fs.readFileSync('/etc/letsencrypt/live/malisresidences.com/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/malisresidences.com/fullchain.pem')
    };
}


let serverToken = process.env.SOCKET_SERVER_TOKEN;

let server = null;
if (process.env.APP_ENV === 'production') {
    server = require('https').Server(options, app);
} else {
    server = require('http').Server(options, app);
}

let io = require('socket.io')(server);
let redis = require('redis');

let helper = require('./common/helper');
let users = require('./model/users');
let comments = require('./model/comments');
let cloudMessaging = require('./firebase/cloudMessaging');
let stickers = require('./model/stickers');
let medias = require('./model/media');
// let activity = require('./model/activities');
let letTalk = require('./model/letTalk');
let userOnline = require('./model/userOnline');
let commentMention = require('./model/commentMentions');
let _ = require('lodash');


// const PORT = process.env.PORT || 4100;
const PORT = process.env.SOCKET_SERVER_PORT;
console.log(`server run on port: ${PORT}`);
server.listen(PORT);
console.log('I am in nodejs...');


const isValid = (token) => {
    return (token === serverToken);
};


let userDatas = {};
//const ioComment = io.of('/comment');
const ioComment = io;

/*ioComment.use((socket, next) => {
    let token = socket.handshake.query.token;
    console.log(token);

    if (isValid(token)) {
        return next();
    }

    return next(new Error('authentication error'));
});*/


let redisClient = redis.createClient(
    {
        port: process.env.REDIS_PORT,               // replace with your port
        host: process.env.REDIS_HOST,        // replace with your hostanme or IP address
        password: process.env.REDIS_PASSWORD,    // replace with your password
        // optional, if using SSL
        // use `fs.readFile[Sync]` or another method to bring these values in
        // tls: {
        //     // key: stringValueOfKeyFile,
        //     // cert: stringValueOfCertFile,
        //     // ca: [stringValueOfCaCertFile]
        //     key: fs.readFileSync('/etc/letsencrypt/live/malisresidences.com/privkey.pem'),
        //     cert: fs.readFileSync('/etc/letsencrypt/live/malisresidences.com/fullchain.pem')
        // }
    }
);

/**
 * Test Redis client send from php to node
 */

redisClient.subscribe('post.join-chart-room');
redisClient.on("message", async (channel, result) => {
    let data = JSON.parse(result);
    let room = `let-talk-${data.let_talk_id}`;

    data = {
        ...data,
        room,
        // caption_type: 'share',
        // is_join_room: true
    };

    console.log(`New user ${data.user_id} joined for ${room} room!`);
    ioComment.in(room).emit('show-info', `New user ${data.user_id} joined for ${room} room!`);
    await letTalkMessage(ioComment, {}, data);
});


ioComment.on('connection', (socket) => {
    console.log("conection ...", socket.id);


    socket.on("register-user", async (data) => {
        socket.user_id = data.user_id;

        if (data.device_id) {
            socket.device_id = data.device_id;
            socket.os = data.os;
            socket.firebase_token = data.firebase_token
        }

        //if (!userDatas[data.user_id]) {
        let resUser = await users.getUser(data.user_id)
        resUser.socket_id = socket.id;
        userDatas[data.user_id] = resUser;
        socket.userData = resUser;

        //}
    });

    socket.on('join-room', async (data) => {

        socket.user_id = data.user_id;
        socket.room = data.room;
        if (data.device_id) {
            socket.device_id = data.device_id;
            socket.os = data.os;
            socket.firebase_token = data.firebase_token
        }


        socket.join(data.room);
        ioComment.in(data.room).emit('show-info', `New user ${socket.user_id} joined for ${data.room} room!`);

        if (data.room.startsWith('let-talk-') && data.user_id) {
            let tmp = data.room.split('let-talk-');
            let letTalkId = tmp[1];
            let userSeens = await letTalk.getUsersSeenLastMessage(letTalkId, data.user_id);
            let found = userSeens.find(item => {
                return item.id == data.user_id;
            });

            if (!found) {
                userSeens.push(socket.userData);
            }

            ioComment.in(data.room).emit(
                "let-talk-users-seen-last-message",
                { user_reads: userSeens }
            );

            ioComment.in(data.room).emit(
                "let-talk-new-user-in-room",
                { user_id: data.user_id }
            );

            socket.let_talk_id = letTalkId;
            letTalk.seenLetTalkMark(letTalkId, data.user_id);

            let onlineUsers = await usersOnlinePrivateLetTalk(userDatas, data.user_id);
            onlineUsers.forEach(async user => {
                if (socket.broadcast.to(user.socket_id)) {
                    socket.broadcast.to(user.socket_id).emit('let-talk-private-status-user-online', {
                        user_id: data.user_id,
                        status: 'online'
                    });
                }
            });
        }

        if (!userDatas[data.user_id]) {
            let resUser = await users.getUser(data.user_id);
            resUser.socket_id = socket.id;
            userDatas[data.user_id] = resUser;
            socket.userData = resUser;
        } else {
            if (!userDatas[data.user_id]['socket_id'])
                userDatas[data.user_id]['socket_id'] = socket.id;
        }

        if (ioComment.sockets.adapter.rooms[data.room] && data.room !== 'list-let-talk-group') {
            var clients = ioComment.sockets.adapter.rooms[data.room].sockets;
            var numClients = (typeof clients !== 'undefined') ? Object.keys(clients).length : 0;
            ioComment.in(data.room).emit('number-of-user-in-room', { total: numClients });

            let clientDatas = [];
            for (var clientId in clients) {
                if (ioComment.sockets.connected[clientId]) {
                    var clientSocket = ioComment.sockets.connected[clientId];
                    if (clientSocket.userData) {
                        clientDatas.push(clientSocket.userData);
                    }
                }
            }

            ioComment.in(data.room).emit('list-users-in-room', { users: clientDatas });
        }

        if (data.room === 'list-let-talk-group') {

            await userOnline.insertNotExisted(data.user_id);
            let onlineUsers = await usersOnlinePrivateLetTalk(userDatas, data.user_id);
            onlineUsers.forEach(async user => {
                if (socket.broadcast.to(user.socket_id)) {
                    socket.broadcast.to(user.socket_id).emit('let-talk-private-status-user-online', {
                        user_id: data.user_id,
                        status: 'online'
                    });
                }

                if (ioComment.sockets.sockets[user.socket_id]) {
                    let onlineUsersEach = await usersOnlinePrivateLetTalk(userDatas, user.id);
                    ioComment.sockets.sockets[user.socket_id].emit("let-talk-group-current-users-online",
                        { users: onlineUsersEach }
                    );
                }

            });
        }

        // Todo debug
        // let socketUsers = helper.getSocketUsers(ioComment, 'list-let-talk-group');
        // console.log('user in list-let-talk-group', socketUsers);

        console.log(`New user ${socket.user_id} joined for android ${data.room} room! socket.id ${socket.id}`);
        ioComment.in(data.room).emit('show-info', `New user ${socket.user_id} joined for android ${data.room} room! socket.id ${socket.id}`);
    });

    socket.on('leave-room', async (data) => {

        ioComment.in(data.room).emit('show-info', `User ${socket.user_id} leaved ${data.room} room!`);
        socket.room = undefined;
        socket.leave(data.room);

        if (ioComment.sockets.adapter.rooms[data.room]) {

            var clients = ioComment.sockets.adapter.rooms[data.room].sockets;
            var numClients = (typeof clients !== 'undefined') ? Object.keys(clients).length : 0;
            ioComment.in(data.room).emit('number-of-user-in-room', { total: numClients });

            let clientDatas = [];
            for (var clientId in clients) {
                if (ioComment.sockets.connected[clientId]) {
                    var clientSocket = ioComment.sockets.connected[clientId];
                    if (clientSocket.userData) {
                        clientDatas.push(clientSocket.userData);
                    }
                }
            }

            ioComment.in(data.room).emit('list-users-in-room', { users: clientDatas });
        }

    });

    socket.on('user-typing', (is_typing) => {
        if (is_typing === true) {
            socket.broadcast.emit('user-typing', {
                user: socket.userData ? socket.userData : "",
                is_typing: true
            });
        } else {
            socket.broadcast.emit('user-typing', {
                user: socket.userData ? socket.userData : "",
                is_typing: false
            });
        }
    });

    socket.on('let-talk-user-typing', (data) => {
        let errors = helper.validate({
            user_id: { required: true },
            room: { required: true },
        }, data);

        if (errors) {
            ioComment.in(data.room).emit('show-info', `Validation error! ${JSON.stringify(errors)}`);
            ioComment.in(data.room).emit('validate-errors', errors);
            return false;
        }

        let isTyping = data.is_typing || false;

        ioComment.in(data.room).emit('let-talk-user-typing', {
            user: socket.userData ? socket.userData : "",
            is_typing: isTyping
        });

    });

    socket.on('let-talk-user-voice-recording', (data) => {

        let errors = helper.validate({
            user_id: { required: true },
            room: { required: true },
        }, data);

        if (errors) {
            ioComment.in(data.room).emit('show-info', `Validation error! ${JSON.stringify(errors)}`);
            ioComment.in(data.room).emit('validate-errors', errors);
            return false;
        }

        let isRecording = data.is_recording || false;

        ioComment.in(data.room).emit('let-talk-user-voice-recording', {
            user: socket.userData ? socket.userData : "",
            is_recording: isRecording
        });

    });

    socket.on('data-error', (data) => {
        console.log("error...");
        console(1000);

    });


    socket.on('comment-message', async (data) => {

        if (!data.caption.trim()) {

            // console.log('---------', data);
            socket.join(data.room);
            socket.user_id = data.user_id;
            socket.room = data.room;
            if (!userDatas[data.user_id]) {
                let resUser = users.getUser(data.user_id);
                resUser.socket_id = socket.id;
                userDatas[data.user_id] = resUser;
                socket.userData = resUser;
            } else {
                if (!userDatas[data.user_id]['socket_id'])
                    userDatas[data.user_id]['socket_id'] = socket.id;
            }

            return false;
        }

        let errors = helper.validate({
            room: { required: true },
            caption: { required: true },
            friend_user_id: { required: true },
            user_id: { required: true },
            type: { required: true },
            comment_id: { required: true }
        }, data);

        // console.log(errors,data);

        if (errors) {
            ioComment.in(data.room).emit('show-info', `Validation error! ${JSON.stringify(errors)}`);
            ioComment.in(data.room).emit('validate-errors', errors);
            return false;
        }

        data.reply_comment_id = !data.reply_comment_id ? data.reply_comment_id : 0;
        data.caption_type = data.caption_type ? data.caption_type.toLowerCase() : 'text';

        let resultComment = await comments.userCommentOnPost(
            data.user_id,
            data.friend_user_id,
            data.comment_id,
            data.type,
            data.caption,
            data.reply_comment_id,
            data.caption_type
        );

        socket.user_id = data.user_id;
        if (!userDatas[data.friend_user_id]) {
            let resUser = await users.getUser(data.friend_user_id);
            userDatas[data.friend_user_id] = resUser;
        } else {
            if (userDatas[data.friend_user_id]['socket_id'])
                userDatas[data.friend_user_id]['socket_id'] = socket.id;
        }

        if (!userDatas[data.user_id]) {
            let resUser = await users.getUser(data.user_id);
            userDatas[data.user_id] = resUser;
        } else {
            if (!userDatas[data.user_id]['socket_id'])
                userDatas[data.user_id]['socket_id'] = socket.id;
        }

        let cmt = await comments.getCommentById(resultComment.insertId);
        let socketUsers = helper.getSocketUsers(ioComment, data.room);

        let userComment = userDatas[data.user_id];
        let resComment = cmt;
        let tmp = resComment["friend_user_id"];
        resComment['friend_user_id'] = resComment["user_id"];
        resComment['user_id'] = tmp;
        resComment['user_username'] = userComment['username'];
        resComment['user_profile_picture'] = userComment['profile_picture'];
        resComment['created_at'] = "just now";
        resComment['caption_type'] = resComment["caption_type"];
        resComment = helper.objectToString(resComment);
        ioComment.in(data.room).emit(
            "comment-message",
            resComment
        );

        if (data.sticker_id) {
            let resSticker = await stickers.getSticker({ sticker_id: data.sticker_id });
            if (resSticker.id) {

                let resStickerLog = await stickers.getUserStickerLog({
                    user_id: data.user_id,
                    sticker_id: data.sticker_id
                });

                if (resStickerLog.id) {

                    stickers.updateRecentUseSticker({
                        user_id: data.user_id,
                        sticker_id: data.sticker_id
                    });

                } else {

                    stickers.insertRecentUseSticker({
                        user_id: data.user_id,
                        sticker_id: data.sticker_id,
                        sticker_category_id: resSticker.sticker_category_id
                    });

                }
            }

        }

        const mentionUserIds = helper.getUserIds(resComment.caption);

        try {
            if (mentionUserIds.length)
                commentMention.insertMultiple({ user_id: data.user_id, mentionUserIds, comment_id: resComment.id });
            // console.log(mentionUserIds);
        } catch (err) {
            console.log(err.message);
        }
        // console.log('end here....');
        // return false;

        let resultToken = await comments.getFirebaseTokenForSendNotification(
            data.friend_user_id,
            data.user_id,
            data.comment_id,
            data.reply_comment_id,
            mentionUserIds
        );

        data = helper.objectToString(data);
        if (data.type == 2) {
            let resMedia = await medias.getMedia(data.comment_id);
            data['post_id'] = resMedia['post_id'] + '';
            data['media_id'] = data['room'];
            cloudMessaging.sendNotification(
                resultToken,
                data,
                userComment,
                userDatas[data.friend_user_id],
                socketUsers
            );
        } else {
            data['post_id'] = data['comment_id'];
            data['media_id'] = '0';
            cloudMessaging.sendNotification(
                resultToken,
                data,
                userComment,
                userDatas[data.friend_user_id],
                socketUsers
            );
        }
    });

    socket.on('let-talk-message', async (data) => {

        await letTalkMessage(ioComment, socket, data);

    });

    socket.on('let-talk-group-seen', async (data) => {

        let errors = helper.validate({
            let_talk_id: { required: true },
            user_id: { required: true }
        }, data);

        if (errors) {
            ioComment.in(data.room).emit('show-info', `Validation error! ${JSON.stringify(errors)}`);
            ioComment.in(data.room).emit('validate-errors', errors);
            return false;
        }

        await letTalk.seenLetTalkMark(data.let_talk_id, data.user_id);
    });

    socket.on('let-talk-group-read-voice', async (data) => {

        let errors = helper.validate({
            let_talk_id: { required: true },
            user_id: { required: true },
            room: { required: true },
            let_talk_message_id: { required: true }
        }, data);

        if (errors) {
            ioComment.in(data.room).emit('show-info', `Validation error! ${JSON.stringify(errors)}`);
            ioComment.in(data.room).emit('validate-errors', errors);
            return false;
        }

        let arrUsers = await letTalk.seenLetTalkMarkVoice(data.let_talk_id, data.user_id, data.let_talk_message_id);

        arrUsers.forEach(userId => {
            // let socketId = null;
            if (userDatas[userId]) {
                if (userDatas[userId].socket_id) {
                    let socketId = userDatas[userId].socket_id;
                    ioComment.sockets.sockets[socketId].emit("let-talk-group-read-voice",
                        { ...data }
                    );
                }
            }
        });

    });

    socket.on('disconnect', async () => {
        if (socket.user_id && socket.let_talk_id) {
            await letTalk.updateLetTalkMember({ user_id: socket.user_id, let_talk_id: socket.let_talk_id });
        }

        if (userDatas[socket.user_id]) {
            delete userDatas[socket.user_id]['socket_id'];
        }

        if (socket.user_id) {

            await userOnline.destroy(parseInt(socket.user_id));
            let onlineUsers = await usersOnlinePrivateLetTalk(userDatas, socket.user_id);

            onlineUsers.forEach(async user => {

                if (socket.broadcast.to(user.socket_id)) {
                    socket.broadcast.to(user.socket_id).emit('let-talk-private-status-user-online', {
                        user_id: socket.user_id,
                        status: 'offline'
                    });
                }

                if (ioComment.sockets.sockets[user.socket_id]) {
                    let onlineUsersEach = await usersOnlinePrivateLetTalk(userDatas, user.id);
                    ioComment.sockets.sockets[user.socket_id].emit("let-talk-group-current-users-online",
                        { users: onlineUsersEach }
                    );
                }
            });
        }


        // Todo Debug
        // let socketUsers = helper.getSocketUsers(ioComment, 'list-let-talk-group');
        // console.log('user in list-let-talk-group', socketUsers);

        console.log(`user disconnected ${socket.user_id} let_talk_id ${socket.let_talk_id} room: ${socket.room} socket.id ${socket.id}`);
        ioComment.emit('show-info', `user disconnected ${socket.user_id} let_talk_id ${socket.let_talk_id} room: ${socket.room} socket.id ${socket.id}`);
    });
});

async function letTalkMessage(ioComment, socket, data) {

    if (data.is_join_room == true) {
        socket.join(data.room);

        if (data.device_id) {
            socket.device_id = data.device_id;
            socket.os = data.os;
            socket.firebase_token = data.firebase_token
        }

        socket.user_id = data.user_id;
        socket.room = data.room;
        if (!userDatas[data.user_id]) {
            let resUser = await users.getUser(data.user_id);
            resUser.socket_id = socket.id;
            userDatas[data.user_id] = resUser;
            socket.userData = resUser;
        } else {
            if (!userDatas[data.user_id]['socket_id'])
                userDatas[data.user_id]['socket_id'] = socket.id;
        }

        // Todo debug
        // let socketUsers = helper.getSocketUsers(ioComment, 'list-let-talk-group');
        // console.log('user in list-let-talk-group', socketUsers);

        if (data.room === 'list-let-talk-group') {
            await userOnline.insertNotExisted(data.user_id);
        }

        let onlineUsers = await usersOnlinePrivateLetTalk(userDatas, data.user_id);

        onlineUsers.forEach(async user => {

            if (socket.broadcast.to(user.socket_id)) {
                socket.broadcast.to(user.socket_id).emit('let-talk-private-status-user-online', {
                    user_id: socket.user_id,
                    status: 'online'
                });
            }

            if (ioComment.sockets.sockets[user.socket_id]) {
                let onlineUsersEach = await usersOnlinePrivateLetTalk(userDatas, user.id);
                ioComment.sockets.sockets[user.socket_id].emit("let-talk-group-current-users-online",
                    { users: onlineUsersEach }
                );
            }
        });

        if (data.let_talk_id && data.user_id) {

            socket.let_talk_id = data.let_talk_id;
            let userSeens = await letTalk.getUsersSeenLastMessage(data.let_talk_id, data.user_id);
            let found = userSeens.find(item => {
                return item.id == data.user_id;
            });

            if (!found) {
                userSeens.push(socket.userData);
            }

            ioComment.in(data.room).emit(
                "let-talk-users-seen-last-message",
                { user_reads: userSeens }
            );

            ioComment.in(data.room).emit(
                "let-talk-new-user-in-room",
                { user_id: data.user_id }
            );

            await letTalk.seenLetTalkMark(data.let_talk_id, data.user_id);
        }

        console.log(`New user ${socket.user_id} joined for ios ${data.room} room! socket.id ${socket.id}`);
        ioComment.in(data.room).emit('show-info', `New user ${socket.user_id} joined for ios ${data.room} room! socket.id ${socket.id}`);
        return false;
    }

    let errors = helper.validate({
        room: { required: true },
        caption: { required: true },
        let_talk_id: { required: true },
        user_id: { required: true }
    }, data);

    if (errors) {
        ioComment.in(data.room).emit('show-info', `Validation error! ${JSON.stringify(errors)}`);
        ioComment.in(data.room).emit('validate-errors', errors);
        return false;
    }

    data.caption_type = data.caption_type ? data.caption_type.toLowerCase() : 'text';
    data.let_talk_type = data.let_talk_type ? data.let_talk_type.toLowerCase() : 'title';
    data.duration = data.duration ? data.duration : '';
    data.width = data.width ? data.width : null;
    data.height = data.height ? data.height : null;
    // data.post_id = data.post_id ? data.post_id : null;
    data.shareable_id = data.shareable_id ? data.shareable_id : null;
    data.shareable_type = data.shareable_id ? 'posts' : null;

    // let check = await letTalk.checkLetTalkMessageExisted(data.user_id, data.let_talk_id);
    // console.log({check});
    // if (check) return false;

    let resMessage = await letTalk.insertMessage(data);

    socket.user_id = data.user_id;
    if (!userDatas[data.user_id]) {
        let resUser = await users.getUser(data.user_id);
        if (socket.id)
            resUser.socket_id = socket.id;
        userDatas[data.user_id] = resUser;
    } else {
        if (userDatas[data.user_id]['socket_id'])
            if (socket.id)
                userDatas[data.user_id]['socket_id'] = socket.id;
    }


    let resLetTalkMessage = await letTalk.getLetTalkMessageById(resMessage.insertId);
    let socketUsers = [];
    let pro = [];
    if (ioComment.sockets.adapter.rooms[data.room]) {
        var clients = ioComment.sockets.adapter.rooms[data.room].sockets;
        for (var clientId in clients) {
            if (ioComment.sockets.connected[clientId]) {
                var clientSocket = ioComment.sockets.connected[clientId];
                if (clientSocket.user_id) {
                    socketUsers.push(clientSocket.user_id);
                    pro.push(letTalk.seenLetTalkMark(data.let_talk_id, clientSocket.user_id));
                }
            }
        }
    }

    await Promise.all(pro);

    if (data.caption_type == 'share') {
        await letTalk.seenLetTalkMark(data.let_talk_id, data.user_id);
    }

    let userSeens = await letTalk.getUsersSeenLastMessage(data.let_talk_id, data.user_id);

    ioComment.in(data.room).emit(
        "let-talk-users-seen-last-message",
        { user_reads: userSeens }
    );

    ioComment.in(data.room).emit(
        "let-talk-group-current-users-online",
        { users: socketUsers }
    );

    let socketUsersListLetTalkGroup = [];
    let listGroupRoom = 'list-let-talk-group';
    if (ioComment.sockets.adapter.rooms[listGroupRoom]) {
        var clients = ioComment.sockets.adapter.rooms[listGroupRoom].sockets;
        for (var clientId in clients) {
            if (ioComment.sockets.connected[clientId]) {
                var clientSocket = ioComment.sockets.connected[clientId];
                if (clientSocket.user_id) {
                    let tmp = {
                        user_id: clientSocket.user_id,
                        os: clientSocket.os,
                        firebase_token: clientSocket.firebase_token,
                        device_id: clientSocket.device_id
                    };
                    socketUsersListLetTalkGroup.push(tmp);
                }
            }
        }
    }

    let resLetTalk = await letTalk.getLetTalkById(data.let_talk_id);
    let userComment = userDatas[data.user_id];
    let resComment = resLetTalkMessage;
    resComment['user_id'] = data.user_id;
    resComment['unique_id'] = data.unique_id;
    resComment['user_username'] = userComment['username'];
    resComment['user_profile_picture'] = userComment['profile_picture'];
    resComment['created_at'] = "just now";
    resComment['let_talk_name'] = resLetTalk['name'];
    resComment['let_talk_photo_url'] = resLetTalk['photo_url'];
    resComment['let_talk_id'] = resLetTalk['id'];
    resComment['shareable_id'] = data.shareable_id;
    resComment['post'] = data.post ? JSON.stringify(data.post) : null;

    if (resComment['let_talk_type'] == 'private') {
        resComment['let_talk_name'] = resComment['user_username'];
        resComment['let_talk_photo_url'] = resComment['user_profile_picture'];
    }

    resComment = helper.objectToString(resComment);
    ioComment.in(data.room).emit(
        "let-talk-message",
        resComment
    );

    // if (data.caption_type == 'share' && data.caption_add) {
    //     delete resComment['post'];
    //     data.shareable_id = null;
    //     data.shareable_type = null;
    //     data.caption = data.caption_add;
    //     delete data.caption_add;
    //
    //     let resMessage = await letTalk.insertMessage(data);
    //     let socketUsers = [];
    //     let pro = [];
    //     if (ioComment.sockets.adapter.rooms[data.room]) {
    //         var clients = ioComment.sockets.adapter.rooms[data.room].sockets;
    //         for (var clientId in clients) {
    //             if (ioComment.sockets.connected[clientId]) {
    //                 var clientSocket = ioComment.sockets.connected[clientId];
    //                 if (clientSocket.user_id) {
    //                     socketUsers.push(clientSocket.user_id);
    //                     pro.push(letTalk.seenLetTalkMark(data.let_talk_id, clientSocket.user_id));
    //                 }
    //             }
    //         }
    //     }
    //
    //     await Promise.all(pro);
    //
    //     if (data.caption_type == 'share') {
    //         await letTalk.seenLetTalkMark(data.let_talk_id, data.user_id);
    //     }
    //
    //     let userSeens = await letTalk.getUsersSeenLastMessage(data.let_talk_id, data.user_id);
    //
    //     ioComment.in(data.room).emit(
    //         "let-talk-users-seen-last-message",
    //         {user_reads: userSeens}
    //     );
    //
    //     resComment['caption'] = data.caption;
    //     resComment['id'] = resMessage.insertId;
    //     ioComment.in(data.room).emit(
    //         "let-talk-message",
    //         resComment
    //     );
    // }

    ioComment.in(`list-${data.room}`).emit('let-talk-group', { let_talk_id: data.let_talk_id });

    if (data.sticker_id) {
        let resSticker = await stickers.getSticker({ sticker_id: data.sticker_id });
        if (resSticker.id) {
            let resStickerLog = await stickers.getUserStickerLog({
                user_id: data.user_id,
                sticker_id: data.sticker_id
            });
            if (resStickerLog.id) {
                stickers.updateRecentUseSticker({
                    user_id: data.user_id,
                    sticker_id: data.sticker_id
                });
            } else {
                stickers.insertRecentUseSticker({
                    user_id: data.user_id,
                    sticker_id: data.sticker_id,
                    sticker_category_id: resSticker.sticker_category_id
                });
            }
        }
    }

    if (data.sticker_id) {
        let resSticker = await stickers.getSticker({ sticker_id: data.sticker_id });
        if (resSticker.id) {

            let resStickerLog = await stickers.getUserStickerLog({
                user_id: data.user_id,
                sticker_id: data.sticker_id
            });

            if (resStickerLog.id) {

                stickers.updateRecentUseSticker({
                    user_id: data.user_id,
                    sticker_id: data.sticker_id
                });

            } else {

                stickers.insertRecentUseSticker({
                    user_id: data.user_id,
                    sticker_id: data.sticker_id,
                    sticker_category_id: resSticker.sticker_category_id
                });

            }
        }

    }

    let resultToken = await comments.getFirebaseTokenLetTalks({ ...data, device_id: socket.device_id });
    data = helper.objectToString(data);
    cloudMessaging.sendLetTalkNotifications(
        resultToken,
        data,
        userComment,
        resComment,
        socketUsers,
        socketUsersListLetTalkGroup
    );
}

async function usersOnlinePrivateLetTalk(userDatas, userId) {
    let usersArray = await letTalk.getLetTalkPrivateFriends(userId);


    let onlineUsers = [];
    usersArray.forEach(userId => {
        let user = userDatas[userId];
        if (user)
            if (user.socket_id)
                onlineUsers.push(user);
    });

    return _.uniqBy(onlineUsers, 'id');

    // return onlineUsers;
}
