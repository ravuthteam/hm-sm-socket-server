let FCM = require("fcm-push");
let frmConfig = require("./fcmConfig");
let activity = require("./../model/activities");
let letTalk = require("./../model/letTalk");
let userSession = require('./../model/userSession');
let helper = require("./../common/helper");
let serverKey = frmConfig.serverKey;
let fcm = new FCM(serverKey);

let sendNotification = async (tokens, dataComment, userComment, ownerPost, socketUsers) => {
        let title = "Notifications";

        // console.log(1111);

        let captionType = dataComment.caption_type;
        let caption = (dataComment.caption_type === 'sticker') ? `"You've got a sticker"` : dataComment.caption;

        let userName = userComment["username"];

        // console.log(tokens);
        // return false;

        for (let i = 0; i < tokens.length; i++) {
            let body = caption;

            let userId = tokens[i]['user_id'];
            let notificationStatus = tokens[i]['notification_status'];
            let type = tokens[i]['type'];
            let token = tokens[i]["firebase_token"];

            let found = false;
            for (let j = 0; j < socketUsers.length; j++) {

                if (userId == socketUsers[j]) {
                    found = true;
                    break;
                }
            }

            if (found == true || notificationStatus == 1) {
                continue;
            }

            if (type == 'comment') {

                if (userComment["reply_comment_id"] == 0) {

                    if (ownerPost["id"] == userId) {

                        title = `${userComment["username"]} replied on your comment.`;
                        // body = `${caption}`;

                    } else {

                        title = `${userComment["username"]} replied on comment you also comment.`;
                        // body = `${caption}`;

                    }

                } else {

                    if (dataComment.user_id == userId) {
                        continue;
                    }

                    if (ownerPost["id"] == userId) {

                        title = `${userName} commented on your post.`;
                        // body = `${caption}`;

                    } else {

                        title = `${userName} commented on a post you also comment.`;
                        if (caption.includes('@#!'))
                            body = `${userName} mention ${helper.getStringMention(caption)}`;
                        // body = `${caption}`;

                    }
                }
            }

            else if (type == 'tag') {

                if (dataComment.user_id == userId) {
                    continue;
                }

                title = `${userName} commented on the post you were tagged in.`;
                // body = `${caption}`;
            }

            else if (type == 'mention') {

                if (dataComment.user_id == userId) {
                    continue;
                }

                title = `${userName} mentioned you in comment.`;
                body = '';

                // if (ownerPost["id"] == userId) {
                //
                //     title = `${userName} mentioned you in comment.`;
                //     body = '';
                //
                // } else {
                //
                //     title = `${userName} commented on a post you also comment.`;
                //     body = `${userName} mention ${helper.getStringMention(caption)}`;
                //
                // }
            }

            let resTotal = await activity.getCountActivitiesUnread(userId);
            let resTotalUnseen = await letTalk.getTotalLetTalkUnRead(userId);

            let badgeTotal = resTotal.total;

            if (body.includes('@#!')) {
                body = helper.getStringMention(body);
            }
            // body = formatMention(body);

            let message = {
                    to: token,
                    notification: {
                        title: title,
                        body: body,
                        click_action: 'read_more_screen',
                        badge: (badgeTotal + resTotalUnseen.total) + '',
                    },
                    android: {
                        priority: "high",
                        badge: (badgeTotal + resTotalUnseen.total) + ''
                    },
                    aps: {
                        alert: {
                            title: title,
                            badge: (badgeTotal + resTotalUnseen.total) + '',
                            body: body,
                        },
                        "mutable-content": 1
                    },
                    data: {
                        title: title,
                        body: body,
                        link: {
                            user_id: ownerPost.id + '',
                            friend_user_id: userComment.id,
                            post_id: dataComment.post_id,
                            media_id: dataComment.media_id,
                            type: dataComment.type,
                            caption_type: captionType,
                            activity_type: 'comment',
                        },
                        total_unread_activity: badgeTotal + '',
                        total_unread_let_talk: resTotalUnseen.total + ''
                    }
                }
            ;

            fcm.send(message, function (err, response) {
                if (err) {
                    console.log("Something has gone wrong!", err);
                    if (err === 'NotRegistered') {
                        userSession.destroy(token);
                    }
                } else {
                    //console.log("Successfully sent with response: ", response);
                    console.log("Successfully sent with response");

                    // if (userId == 39) {
                    //     console.log(message);
                    // }

                }
            });

        }
    }
;

let sendLetTalkNotifications = async (tokens,
                                      dataComment,
                                      userComment,
                                      resComment,
                                      socketUsers,
                                      socketUsersListLetTalkGroup) => {

    let title = resComment.let_talk_name;
    let captionType = dataComment.caption_type;
    let letTalkType = resComment.let_talk_type;
    let caption = formatCaption(userComment, {...dataComment, letTalkType});

    if (caption.includes('@#!'))
        caption = helper.getStringMention(caption);

    // console.log({
    //     socketUsers,
    //     socketUsersListLetTalkGroup
    // });
    //
    for (let i = 0; i < tokens.length; i++) {

        let firebaseToken = tokens[i]['firebase_token'];
        // let os = tokens[i]['os'];
        let deviceId = tokens[i]['device_id'];
        let userId = tokens[i]['user_id'];

        let found = socketUsers.find(itemUserId => userId == itemUserId);
        if (found || userComment.id == userId)
            continue;

        let resTotal = await activity.getCountActivitiesUnread(userId);
        let resTotalUnseen = await letTalk.getTotalLetTalkUnRead(userId);

        // console.log({resTotal, resTotalUnseen, userId});

        let badgeTotal = resTotal.total;

        let message = {
            to: firebaseToken,
            notification: {
                title: title,
                body: caption,
                click_action: 'let_talk',
                badge: (badgeTotal + resTotalUnseen.total) + '',
                sound: 'default',
            },
            android: {
                priority: "high",
                badge: (badgeTotal + resTotalUnseen.total) + '',
                sound: 'default',
            },
            aps: {
                alert: {
                    title: title,
                    badge: (badgeTotal + resTotalUnseen.total) + '',
                    body: caption,
                    sound: 'default',
                },
                "mutable-content": 1
            },
            data: {
                title: title,
                body: caption,
                link: {
                    user_id: userComment.id + '',
                    caption_type: captionType,
                    activity_type: 'let_talk',
                    let_talk_id: dataComment.let_talk_id,
                    let_talk_name: resComment['let_talk_name'],
                    let_talk_photo_url: resComment['let_talk_photo_url'],
                    caption: caption
                },
                total_unread_activity: badgeTotal + '',
                total_unread_let_talk: resTotalUnseen.total + ''
            }
        };

        // console.log(message);
        fcm.send(message, function (err, response) {
            if (err) {
                console.log("Something has gone wrong!");
                // user.deleteUserToken(token);
            } else {
                // console.log("Successfully sent with response");
            }
        });

        // if (os == 'ios') {
        // console.log({socketUsersListLetTalkGroup, userId, deviceId});

        let foundListGroup = socketUsersListLetTalkGroup.find(item => {
            return (item.device_id == deviceId && item.os == 'ios' && item.user_id == userId);
        });

        // console.log({foundListGroup});

        if (foundListGroup) {

            console.log('...send...');

            message.content_available = true;

            fcm.send(message, function (err, response) {
                if (err) {
                    console.log("Something has gone wrong!");
                    // user.deleteUserToken(token);
                } else {
                    console.log("Successfully sent with response");
                }
            });
        }
        // }
    }
}


function formatCaption(user, comment) {
    let caption = comment['caption'];
    let caption_type = comment.caption_type;
    let letTalkType = comment.letTalkType;

    if (caption_type == 'sticker')
        caption = 'sent a sticker';
    else if (caption_type == 'photo')
        caption = 'sent a photo';
    else if (caption_type == 'voice')
        caption = 'sent a voice message';
    else if (caption_type == 'video')
        caption = 'sent a video';
    else if (caption_type == 'share')
        caption = 'Sent an attachment.';

    if (letTalkType == 'private')
        return caption;

    return user.username + ' : ' + caption;
}

function formatMention(caption) {
    if (caption.includes('@#!')) {
        return 'Mention you in post.'
    }
    return caption;
}

module.exports = {
    sendNotification,
    sendLetTalkNotifications
};

