const _ = require('lodash');

let empty = function (item) {

    if (item === undefined || item === null || item === '' || Number(item) === 0) {
        return true;
    }

    return false;
};

let objectToString = function (data) {
    let tmp = {};
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
        tmp[keys[i]] = '' + data[keys[i]];
    }
    return tmp;
};

let getSocketUsers = (ioComment, room) => {
    let socketUsers = [];
    if (ioComment.sockets.adapter.rooms[room]) {
        var clients = ioComment.sockets.adapter.rooms[room].sockets;
        for (var clientId in clients) {
            if (ioComment.sockets.connected[clientId]) {
                var clientSocket = ioComment.sockets.connected[clientId];
                if (clientSocket.user_id) {
                    socketUsers.push(clientSocket.user_id);
                }
            }
        }
    }

    return socketUsers;
};


let isValidEmail = email => {
    return !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email);
};

let validate = (rules, values) => {
    let errors = {};
    values = values ? values : {};

    for (let field in rules) {
        let rule = rules[field];
        let type = rule["type"];
        let value = "";
        if (!_.isNil(values[field]))
            if (_.isString(values[field])) value = values[field].trim();
            else value = values[field];
        let required = rule["required"] ? true : false;
        let existed = rule["existed"] ? true : false;
        let equalsTo = rule["equalsTo"] ? rule["equalsTo"] : null;
        let min = rule["min"] ? rule["min"] : null;
        let max = rule["max"] ? rule["max"] : null;
        let minNumber = rule["minNumber"] ? rule["minNumber"] : null;
        let maxNumber = rule["maxNumber"] ? rule["maxNumber"] : null;
        let accepts = rule["accepts"] ? rule["accepts"] : [];

        if (required && !value) {
            if (value !== false)
                errors[field] = rule["message"] || "This field is required";
        }

        if (existed && value) {
            errors[field] = rule["message"] || "This field value already token";
        }

        if (accepts.length && value) {
            if (!_.includes(accepts, value))
                errors[field] =
                    rule["message"] || `This field accept only ${accepts.toString()}`;
        }

        if (max && value) {
            if (value.length > max)
                errors[field] =
                    rule["message"] || `This field maximum ${max} character`;
        }

        if (min && value) {
            if (value.length < min)
                errors[field] =
                    rule["message"] || `This field is at least ${min} character`;
        }

        if (maxNumber && value) {
            if (value > maxNumber)
                errors[field] =
                    rule["message"] || `This field is bigger than ${maxNumber}`;
        }

        if (minNumber && value) {
            if (value < minNumber)
                errors[field] =
                    rule["message"] || `This field is smaller than ${minNumber}`;
        }

        if (equalsTo && value) {
            if (values[equalsTo] != value) {
                const message =
                    equalsTo.message || `${field} and ${equalsTo} do not match!`;
                errors[field] = message;
                errors[equalsTo] = message;
            }
        }

        if (required && type === "email" && value && isValidEmail(value)) {
            errors[field] = rule["message"] || "Invalid email address";
        }
    }

    return Object.keys(errors).length ? errors : null;
};

const getUserIds = (str, limiter = '@#!') => {
    let strs = str.split(limiter);
    // console.log({ strs });
    return strs.filter(item => {
        try {
            return JSON.parse(item);
        } catch (err) {
            return false
        }
    }).map(item => JSON.parse(item))
        .map(item => Number(item.user_id))
};

const getStringMention = (str, limiter = '@#!') => {
    let strs = str.split(limiter);
    return strs.map(item => {
        try {
            let x = JSON.parse(item);
            return x.name
        } catch (err) {
            return item
        }
    }).join('');
};

module.exports = {
    empty,
    objectToString,
    getSocketUsers,
    validate,
    getUserIds,
    getStringMention
};
