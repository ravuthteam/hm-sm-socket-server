let mysql = require("mysql");

const connection = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
});

// const connection = mysql.createConnection({
//   host: "malisresidences.com",
//   user: "lee.ngeap",
//   password: "4163588452ca",
//   database: "hm_sms"
// });


// connection.connect(function (err) {
//     if (err) throw err;
//     console.log("Connected!");
// });

exports.connection = connection;
