let mySqlConnection = require('./mysqlConnection');
let con = mySqlConnection.connection;

let getMedia = (userId, postId) => {

  return new Promise((resolve, reject) => {

    let sql = "SELECT post_id FROM medias WHERE id = ? LIMIT 0,1";
    con.query(sql, [userId, postId], function (err, result, fields) {

      if (err) reject(err);

      let data = {};
      if (result[0] !== undefined) {
        data = Object.assign({}, result[0]);
      }
      resolve(data);
    });
  });
};

module.exports = {
  getMedia,
};