let mySqlConnection = require("./mysqlConnection");
let con = mySqlConnection.connection;

let insertRecentUseSticker = ({ sticker_id, user_id, sticker_category_id }) => {

  return new Promise((resolve, reject) => {
    let sql = `INSERT 
                INTO 
                  user_sticker_logs(user_id,sticker_id,sticker_category_id,created_at,updated_at) 
                  VALUES(?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

    con.query(
      sql,
      [user_id, sticker_id, sticker_category_id],
      function (err, result) {
        if (err) reject(err);

        resolve(result);
      }
    );
  });
};

let getSticker = ({ sticker_id }) => {

  return new Promise((resolve, reject) => {
    let sql = `SELECT * FROM stickers WHERE id = ? LIMIT 0,1`;
    con.query(sql, [sticker_id], function (err, result, fields) {
      if (err) reject(err);

      let sticker = {};
      if (result[0])
        if (result[0] !== undefined) {
          sticker = Object.assign({}, result[0]);
        }
      resolve(sticker);
    });
  });
};

let getUserStickerLog = ({ sticker_id, user_id }) => {
  return new Promise((resolve, reject) => {
    let sql = `SELECT * FROM user_sticker_logs WHERE sticker_id = ? AND user_id = ? LIMIT 0,1;`;
    con.query(sql, [sticker_id, user_id], function (err, result, fields) {
      if (err) reject(err);

      let sticker = {};
      if (result[0])
        if (result[0] !== undefined) {
          sticker = Object.assign({}, result[0]);
        }
      resolve(sticker);
    });
  });
};

let updateRecentUseSticker = ({ sticker_id, user_id }) => {

  return new Promise((resolve, reject) => {
    let sql = `UPDATE 
                 user_sticker_logs
                 SET created_at = UTC_TIMESTAMP()
                 WHERE sticker_id = ? AND user_id = ?;`;

    con.query(
      sql,
      [sticker_id, user_id],
      function (err, result) {
        if (err) reject(err);

        resolve(result);
      }
    );
  });
};

module.exports = {
  insertRecentUseSticker,
  getSticker,
  getUserStickerLog,
  updateRecentUseSticker
};
