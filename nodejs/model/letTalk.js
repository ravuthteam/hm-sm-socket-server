let mySqlConnection = require("./mysqlConnection");
let con = mySqlConnection.connection;

let checkLetTalkMessageExisted = (userId, letTalkId) => {

    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM let_talk_messages WHERE user_id = ? AND let_talk_id = ? AND created_at = UTC_TIMESTAMP() LIMIT 0,1";

        con.query(sql, [userId, letTalkId], function (err, result, fields) {
            if (err) reject(err);

            let isExisted = false;
            if (result[0] !== undefined) {
                isExisted = true;
            }
            resolve(isExisted);
        });
    });
};

let insertMessage = (data) => {
    return getLetTalkById(data.let_talk_id).then(res => {
        return new Promise((resolve, reject) => {
            let sql = `INSERT INTO let_talk_messages(
                  user_id,
                  let_talk_id,
                  caption,
                  caption_type,
                  let_talk_type,
                  duration,
                  shareable_id,
                  shareable_type,
                  width,
                  height,
                  created_at,updated_at)
               VALUES (?,?,?,?,?,?,?,?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

            con.query(
                sql,
                [
                    data.user_id,
                    data.let_talk_id,
                    data.caption,
                    data.caption_type,
                    res.let_talk_type,
                    data.duration,
                    data.shareable_id,
                    data.shareable_type,
                    data.width,
                    data.height
                ],
                function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                }
            );
        });
    });
};


let updateLetTalkMember = (data) => {

    return new Promise((resolve, reject) => {
        let sql = `UPDATE 
                      let_talk_members 
                    SET
                      online_at = UTC_TIMESTAMP() 
                    WHERE friend_user_id = ? 
                      AND let_talk_id = ? ;`;

        con.query(
            sql,
            [
                data.user_id,
                data.let_talk_id
            ],
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });

};

let getLetTalkMessageById = id => {

    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM let_talk_messages WHERE id = ? LIMIT 0,1";
        con.query(sql, [id], function (err, result, fields) {
            if (err) reject(err);

            let msg = {};
            if (result[0] !== undefined) {
                msg = Object.assign({}, result[0]);
            }
            resolve(msg);
        });
    });
};

let insertLetTalkMark = (data) => {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO let_talk_marks(
                  user_id,
                  let_talk_id,
                  let_talk_message_id,
                  created_at,updated_at)
               VALUES (?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

        con.query(
            sql,
            [
                data.user_id,
                data.let_talk_id,
                data.let_talk_message_id
            ],
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });

};


let insertLetTalkMarkVoice = (data) => {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO let_talk_mark_voices(
                  user_id,
                  let_talk_id,
                  let_talk_message_id,
                  created_at,updated_at)
               VALUES (?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

        con.query(
            sql,
            [
                data.user_id,
                data.let_talk_id,
                data.let_talk_message_id
            ],
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });
};

let updateLetTalkMark = (data) => {
    return new Promise((resolve, reject) => {
        let sql = `UPDATE let_talk_marks SET let_talk_message_id = ? WHERE user_id = ? AND let_talk_id = ?;`;

        con.query(
            sql,
            [
                data.let_talk_message_id,
                data.user_id,
                data.let_talk_id
            ],
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });

};

let getLatestMessageLetTalk = letTalkId => {

    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM let_talk_messages WHERE let_talk_id = ? ORDER BY id DESC LIMIT 0,1;";
        con.query(sql, [letTalkId], function (err, result, fields) {
            if (err) reject(err);

            let msg = {};
            if (result[0] !== undefined) {
                msg = Object.assign({}, result[0]);
            }
            resolve(msg);
        });
    });
};


let getLetTalkPrivateFriends = userId => {

        return new Promise((resolve, reject) => {
            //     let sql = `
            //     SELECT
            //       *
            //     FROM
            //       view_let_talk_members
            //     WHERE friend_user_id = ?
            //       AND let_talk_type = 'private'
            // `;

            let sql = `
            SELECT 
              * 
            FROM
              view_let_talk_members 
            WHERE (user_id = ? AND let_talk_type = 'private') OR (friend_user_id = ? AND let_talk_type = 'private') GROUP BY user_id
        `;

            con.query(sql, [userId, userId], function (err, result, fields) {
                if (err) reject(err);

                let data = [userId];
                if (result[0] !== undefined) {
                    for (let i = 0; i < result.length; i++) {
                        data.push(result[i]["user_id"] + '');
                    }
                }
                resolve(data);
            });
        });
    }
;
/*
let getLetTalkPrivateFriends = userId => {

        return new Promise((resolve, reject) => {
            let sql = `
            SELECT
              *
            FROM
              view_let_talk_members
            WHERE friend_user_id = ?
              AND let_talk_type = 'private'
        `;

            con.query(sql, [userId], function (err, result, fields) {
                if (err) reject(err);

                let data = [userId];
                if (result[0] !== undefined) {
                    for (let i = 0; i < result.length; i++) {
                        data.push(result[i]["user_id"] + '');
                    }
                }
                resolve(data);
            });
        });
    }
;*/


let getLetTalkMark = (letTalkId, userId) => {

    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM let_talk_marks WHERE  let_talk_id = ? AND user_id = ? ORDER BY id DESC LIMIT 0,1;";
        con.query(sql, [letTalkId, userId], function (err, result, fields) {
            if (err) reject(err);

            let msg = {};
            if (result[0] !== undefined) {
                msg = Object.assign({}, result[0]);
            }
            resolve(msg);
        });
    });
};


let getLetTalkMarkVoice = (letTalkId, userId, letTalkMessageId) => {

    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM let_talk_mark_voices WHERE  let_talk_id = ? AND user_id = ? AND let_talk_message_id = ? LIMIT 0,1;";
        con.query(sql, [letTalkId, userId, letTalkMessageId], function (err, result, fields) {
            if (err) reject(err);

            let msg = {};

            // console.log({result});
            if (result) {
                msg = Object.assign({}, result[0]);
            }
            resolve(msg);
        });
    });
};


let getOne = (sql, inject = []) => {
    return new Promise((resolve, reject) => {
        con.query(sql, inject, function (err, result, fields) {
            if (err) reject(err);
            let msg = {};
            if (result[0] !== undefined) {
                msg = Object.assign({}, result[0]);
            }
            resolve(msg);
        });
    });
};


let getUsersSeenLastMessage = (letTalkId, userId) => {

    return new Promise((resolve, reject) => {
        let sql = `SELECT 
          u.id,
          u.username,
          u.profile_picture,
          m.let_talk_id 
        FROM
          let_talk_marks AS m 
          JOIN users AS u 
            ON m.user_id = u.id 
        WHERE let_talk_id = ? 
          AND let_talk_message_id = 
          (SELECT 
            max_let_talk_message_id 
          FROM
            view_let_talk_members 
          WHERE friend_user_id = ? 
            AND let_talk_id = ?
          LIMIT 1);`;

        con.query(sql, [letTalkId, userId, letTalkId], function (err, result, fields) {
            if (err) reject(err);

            let data = [];

            if (result[0] !== undefined) {
                for (let i = 0; i < result.length; i++) {
                    let tmp = {
                        id: result[i]["id"],
                        username: result[i]["username"],
                        profile_picture: result[i]["profile_picture"],
                    };
                    data.push(tmp);
                }
            }
            resolve(data);
        });
    });
};

let getTotalLetTalkUnRead = (userId) => {

    return new Promise((resolve, reject) => {
        let sql = `
            SELECT 
              COUNT(*) AS total 
            FROM
              view_let_talk_members 
            WHERE friend_user_id = ? 
              AND (
                max_let_talk_message_id > read_let_talk_message_id 
                AND max_let_talk_message_id > remove_let_talk_message_id
              )`;

        con.query(sql, [userId], function (err, result, fields) {
            if (err) reject(err);

            let msg = {};
            if (result[0] !== undefined) {
                msg = Object.assign({}, result[0]);
            }
            resolve(msg);
        });
    });
};

let seenLetTalkMark = async (letTalkId, userId) => {

    try {
        let letTalkMark = await  getLetTalkMark(letTalkId, userId);
        let letTalkMessage = await  getLatestMessageLetTalk(letTalkId);

        let markMessageId = 0;
        if (letTalkMark.let_talk_message_id) {
            markMessageId = letTalkMark.let_talk_message_id;
        }

        let letTalkMessageId = 0;
        if (letTalkMessage.id) {
            letTalkMessageId = letTalkMessage.id;
        }

        if (markMessageId && letTalkMessageId) {
            // update
            await updateLetTalkMark({
                user_id: userId,
                let_talk_id: letTalkId,
                let_talk_message_id: letTalkMessageId
            })
        }

        else if (!markMessageId && letTalkMessageId) {
            await  insertLetTalkMark({
                let_talk_id: letTalkId,
                user_id: userId,
                let_talk_message_id: letTalkMessageId
            });
        }


    } catch (err) {
        console.log(err);
    }
};


let seenLetTalkMarkVoice = async (letTalkId, userId, letTalkMessageId) => {

    try {
        let letTalkMarkVoice = await  getLetTalkMarkVoice(letTalkId, userId, letTalkMessageId);
        let letTalkMessage = await  getLetTalkMessageById(letTalkMessageId * 1);

        // console.log({letTalkMarkVoice, letTalkMessage, letTalkId, userId, letTalkMessageId})

        if (letTalkMarkVoice.let_talk_message_id || letTalkMessage.user_id == userId) {
            return [];
        }

        await  insertLetTalkMarkVoice({
            let_talk_id: letTalkId,
            user_id: userId,
            let_talk_message_id: letTalkMessageId
        });

        if (letTalkMessage.user_id) {
            let letTalkMarkVoice = await  getLetTalkMarkVoice(letTalkId, letTalkMessage.user_id, letTalkMessageId);
            if (letTalkMarkVoice.let_talk_message_id) {
                return [];
            }
            await  insertLetTalkMarkVoice({
                let_talk_id: letTalkId,
                user_id: letTalkMessage.user_id,
                let_talk_message_id: letTalkMessageId
            });

            return [letTalkMessage.user_id, userId]
        }

        return [userId];
    } catch (err) {
        console.log(err);
    }

};

let getLetTalkById = id => {
    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM let_talks WHERE id = ? LIMIT 0,1";
        con.query(sql, [id], function (err, result, fields) {
            if (err) reject(err);

            let res = [];
            if (result[0] !== undefined) {
                res = Object.assign({}, result[0]);
            }
            resolve(res);
        });
    });
};

module.exports = {
    // getLetTalkMessage,
    insertMessage,
    getLetTalkMessageById,
    getLetTalkById,
    getLatestMessageLetTalk,
    getLetTalkMark,
    insertLetTalkMark,
    seenLetTalkMark,
    getTotalLetTalkUnRead,
    getUsersSeenLastMessage,
    updateLetTalkMember,
    getLetTalkPrivateFriends,
    checkLetTalkMessageExisted,
    seenLetTalkMarkVoice,
    getLetTalkMarkVoice
};
