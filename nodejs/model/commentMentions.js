let mySqlConnection = require("./mysqlConnection");
let con = mySqlConnection.connection;

let insertMultiple = ({user_id, mentionUserIds, comment_id}) => {

    // let {userId, mentionUserIds, postId, type} = data;
    // console.log(data);
    // return 1;
    return new Promise((resolve, reject) => {

        // const {userId,friendUserIds,postId,type} = data;
        let strValue = [];
        let str = `(
                        ?,
                        ?,
                        ?,
                        UTC_TIMESTAMP(),
                        UTC_TIMESTAMP()
                      )`;

        let injection = [];
        for (let mentionId  of mentionUserIds) {
            injection = [...injection, user_id, mentionId, comment_id];
            strValue = [...strValue, str];
        }

        let sql = `INSERT INTO comment_mentions (
                      user_id,
                      friend_user_id,
                      comment_id,
                      created_at,
                      updated_at
                    ) 
                    VALUES${strValue.join(',')}`;

        con.query(
            sql,
            injection,
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });
};
//
// let destroy = (token) => {
//     return new Promise((resolve, reject) => {
//         let sql = `DELETE FROM user_sessions WHERE firebase_token = ?;`;
//
//         con.query(
//             sql,
//             [token],
//             function (err, result) {
//                 if (err) reject(err);
//                 resolve(result);
//             }
//         );
//     });
// };
//
// let findBy = (userId) => {
//     return new Promise((resolve, reject) => {
//         let sql = `SELECT * FROM user_sessions WHERE user_id = ? LIMIT 0,1`;
//         con.query(sql, [userId], function (err, result, fields) {
//             if (err) reject(err);
//
//             let data = {};
//             if (result[0]) {
//                 data = Object.assign({}, result[0]);
//             }
//             resolve(data);
//         });
//     });
// };


// let insertNotExisted = async (userId) => {
//     let userOnline = await findBy(userId);
//     if (!userOnline.user_id)
//         await insert(userId);
// };

module.exports = {
    // destroy,
    // findBy,
    // insertNotExisted,
    insertMultiple
};
