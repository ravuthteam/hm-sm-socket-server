let mySqlConnection = require("./mysqlConnection");
let helpers = require("../common/helper");
let con = mySqlConnection.connection;

let getUsersCommentPost = (userId, postId) => {
    return new Promise((resolve, reject) => {
        let sql =
            "SELECT friend_user_id FROM comments WHERE user_id = ? AND commentable_id = ? GROUP BY friend_user_id";
        con.query(sql, [userId, postId], function (err, result, fields) {

            if (err)
                reject(err);

            let data = [];
            for (let i = 0; i < result.length; i++) {
                data.push(result[i]["friend_user_id"]);
            }
            resolve(data);
        });
    });
};
/*
let userCommentOnPost = (userId,
                         actionUserId,
                         commentableId,
                         commentableType,
                         caption,
                         replyCommentId) => {
  return new Promise((resolve, reject) => {
    let sql = `INSERT INTO comments(friend_user_id,user_id,commentable_id,commentable_type,caption,reply_comment_id,created_at,updated_at)
                   VALUES (?,?,?,?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

    con.query(
        sql,
        [userId, actionUserId, commentableId, commentableType, caption, replyCommentId],
        function (err, result) {

          if (err) reject(err);

          resolve(result);
        }
    );
  });
};*/

let userCommentOnPost = (userId,
                         actionUserId,
                         commentableId,
                         commentableType,
                         caption,
                         replyCommentId,
                         captionType) => {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO comments(friend_user_id,user_id,commentable_id,commentable_type,caption,reply_comment_id,
                  caption_type,created_at,updated_at)
                   VALUES (?,?,?,?,?,?,?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

        con.query(
            sql,
            [userId, actionUserId, commentableId, commentableType, caption, replyCommentId, captionType],
            function (err, result) {

                if (err) reject(err);

                resolve(result);
            }
        );
    });
};

let getChatRooms = () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT id FROM posts WHERE status = 1 GROUP BY id;";
        con.query(sql, function (err, result, fields) {
            if (err) reject(err);

            let data = [];
            for (let i = 0; i < result.length; i++) {
                data.push(result[i]["id"] + "");
            }
            resolve(data);
        });
    });
};


let getChatRoomsForUser = userId => {
    return new Promise((resolve, reject) => {
        let sql =
            `
      SELECT id AS commentable_id FROM posts WHERE user_id = ? AND STATUS = 1
      UNION
      SELECT commentable_id FROM comments WHERE user_id = ? OR friend_user_id = ? GROUP BY commentable_id `;
        con.query(sql, [userId, userId, userId], function (err, result, fields) {
            if (err) reject(err);

            let data = [];
            for (let i = 0; i < result.length; i++) {
                data.push(result[i]["commentable_id"]);
            }
            resolve(data);
        });
    });
};

let getFirebaseTokenForSendNotification = (userId, friendUserId, postId, replyCommentId, mentionUserIds) => {
    return new Promise((resolve, reject) => {

        postId = Number(postId);
        let rejection = [postId, postId, postId, postId];

        let mentionQuery = '';
        if (mentionUserIds.length) {
            rejection.splice(1, 0, mentionUserIds);
            // rejection = [...rejection, postId, mentionUserIds];
            mentionQuery = ` 
            (SELECT 
              user_id, 
              firebase_token, 
              'mention' AS type
                 FROM   user_sessions 
                 WHERE  user_id IN (?) AND firebase_token IS NOT NULL              
            )
            UNION
            `;
        }


        let sql = `
    (SELECT *, 
          CASE
            WHEN  EXISTS (SELECT id FROM post_notifications AS pn WHERE pn.user_id = tmp.user_id AND pn.post_id = ? AND pn.status = 1) THEN 1
            ELSE 0
          END AS notification_status
    FROM   (
         ${mentionQuery}
            (SELECT user_id, 
                    firebase_token, 
                    'tag' AS type 
             FROM   user_sessions 
             WHERE  user_id IN (SELECT post_tags.friend_user_id 
                                FROM   post_tags 
                                WHERE  post_tags.post_id = ?)) 
            UNION 
            (SELECT user_id, 
                    firebase_token, 
                    'comment' AS type 
             FROM   user_sessions 
             WHERE  user_id IN (SELECT user_id 
                                FROM   comments 
                                WHERE  commentable_id = ? 
                                UNION 
                                SELECT friend_user_id 
                                FROM   comments 
                                WHERE  commentable_id = ?) 
             ORDER  BY created_at DESC 
             LIMIT  0, 100)
           ) AS tmp 
    WHERE  firebase_token IS NOT NULL 
    GROUP  BY user_id, 
              firebase_token)
    `;

        if (replyCommentId != 0) {
            rejection = [postId, postId, postId];
            if (mentionUserIds.length) {
                rejection.splice(1, 0, mentionUserIds);
                // rejection = [...rejection, postId, mentionUserIds];
            }

            sql = `
      (SELECT *, 
          CASE
            WHEN  EXISTS (SELECT id FROM post_notifications AS pn WHERE pn.user_id = tmp.user_id AND pn.post_id = ? AND pn.status = 1) THEN 1
            ELSE 0
          END AS notification_status
      FROM   (
           ${mentionQuery}
            (SELECT user_id, 
                    firebase_token, 
                    'comment' AS type 
             FROM   user_sessions 
             WHERE  user_id IN (SELECT user_id 
                                FROM   comments 
                                WHERE  commentable_id = ? 
                                UNION 
                                SELECT friend_user_id 
                                FROM   comments 
                                WHERE  commentable_id = ?) 
             ORDER  BY created_at DESC 
             LIMIT  0, 100)
           ) AS tmp 
    WHERE  firebase_token IS NOT NULL 
    GROUP  BY user_id, 
              firebase_token)
      `;
        }

        // console.log(rejection, sql);

        con.query(sql, rejection, function (err, result, fields) {
            if (err) reject(err);

            let data = [];
            if (result !== undefined) {
                for (let i = 0; i < result.length; i++) {
                    if (result[i]["firebase_token"] != null) {
                        let tmp = {
                            user_id: result[i]["user_id"],
                            firebase_token: result[i]["firebase_token"],
                            type: result[i]["type"],
                            notification_status: result[i]["notification_status"]
                        };
                        data.push(tmp);
                    }
                }
            }
            // console.log('data.length => ',data.length);
            // console.log(data);
            resolve(data);
        });
    });
};

//
// let getFirebaseTokenForSendNotification = (userId,friendUserId, postId) => {
//   return new Promise((resolve, reject) => {
//
//     let sql = `SELECT user_id,
//                       firebase_token
//                 FROM user_sessions
//                 WHERE firebase_token IS NOT NULL AND user_id IN (SELECT friend_user_id
//                                    FROM   comments
//                                    WHERE  user_id = ? AND friend_user_id <> ?
//                                           AND commentable_id = ?
//                                    GROUP  BY friend_user_id) ORDER BY created_at DESC LIMIT 0,100;`;
//
//     con.query(sql, [userId, friendUserId, postId], function (err, result, fields) {
//       if (err) reject(err);
//
//       let data = [];
//       if (result !== undefined) {
//         for (let i = 0; i < result.length; i++) {
//           if (result[i]["firebase_token"] != null) {
//             let tmp = {
//               user_id: result[i]["user_id"],
//               firebase_token: result[i]["firebase_token"]
//             };
//             data.push(tmp);
//           }
//         }
//       }
//       resolve(data);
//     });
//   });
// };


let getCommentById = userId => {
    return new Promise((resolve, reject) => {
        let sql =
            "SELECT * FROM comments WHERE id = ? LIMIT 0,1";
        con.query(sql, [userId], function (err, result, fields) {
            if (err) reject(err);

            let comment = [];
            if (result[0] !== undefined) {
                comment = Object.assign({}, result[0]);
            }
            resolve(comment);
        });
    });
};


let getFirebaseTokenLetTalks = (data) => {
    return new Promise((resolve, reject) => {

        //     let rejection = [data.device_id, data.let_talk_id];
        //
        //     let sql = `
        // SELECT * FROM   user_sessions
        //   WHERE  firebase_token IS NOT NULL
        //          AND (device_id <> ?)
        //          AND user_id IN (SELECT friend_user_id FROM let_talk_members WHERE let_talk_id = ?  AND (UTC_TIMESTAMP() >= mute_end_date OR mute_type = 0))
        //   GROUP  BY user_id,
        //           firebase_token
        // `;


        let rejection = [data.device_id || 'N/A', data.let_talk_id];

        let sql = `
    SELECT * FROM   user_sessions
      WHERE  firebase_token IS NOT NULL 
             AND (device_id <> ?)
             AND user_id IN (SELECT friend_user_id FROM let_talk_members WHERE let_talk_id = ?  AND (UTC_TIMESTAMP() >= mute_end_date OR mute_type = 0))
      GROUP  BY user_id, 
              firebase_token
    `;

        // console.log({sql, rejection, data})

        con.query(sql, rejection, function (err, result, fields) {
            if (err) reject(err);

            let data = [];
            if (result !== undefined) {
                for (let i = 0; i < result.length; i++) {
                    if (result[i]["firebase_token"] != null) {
                        let tmp = {
                            user_id: result[i]["user_id"],
                            firebase_token: result[i]["firebase_token"],
                            os: result[i]["os"],
                            device_id: result[i]["device_id"]
                        };
                        data.push(tmp);
                    }
                }
            }

            // console.log(data)
            resolve(data);
        });
    });
};

module.exports = {
    getUsersCommentPost,
    userCommentOnPost,
    getChatRooms,
    getChatRoomsForUser,
    getFirebaseTokenForSendNotification,
    getCommentById,
    getFirebaseTokenLetTalks
};
