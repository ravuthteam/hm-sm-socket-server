let mySqlConnection = require('./mysqlConnection');
let con = mySqlConnection.connection;

let getCountActivitiesUnread = userId => {
    return new Promise((resolve, reject) => {
        let sql = `SELECT COUNT(*) AS total FROM (SELECT * FROM view_activities WHERE CASE 
                          WHEN activity_type = 'tag' THEN user_id <> ${userId} AND friend_user_id = ${userId} AND post_id IN (SELECT pt.post_id FROM post_tags AS pt WHERE pt.friend_user_id = ${userId})
                          WHEN activity_type = 'comment' THEN friend_user_id <> ${userId} AND (post_id IN (SELECT pt.post_id FROM post_tags AS pt WHERE pt.friend_user_id = ${userId}) OR post_id IN (SELECT cm.commentable_id FROM comments AS cm WHERE cm.friend_user_id = ${userId} OR cm.user_id = ${userId}))
                          WHEN activity_type = 'like' THEN friend_user_id <> ${userId} AND (post_id IN (SELECT pt.post_id FROM post_tags AS pt WHERE pt.friend_user_id = ${userId}) OR post_id IN (SELECT pt.likeable_id FROM likes AS pt WHERE pt.user_id = ${userId}))
                            AND CASE
                                WHEN type = 2 THEN FALSE
                                ELSE TRUE
                            END
                          WHEN activity_type = 'friend' THEN (friend_user_id = ${userId} AND friend_status = 2) OR (user_id = ${userId} AND friend_status = 1) 
                          WHEN activity_type = 'follow' THEN friend_user_id = ${userId} 
                          WHEN activity_type = 'let_talk_message' THEN friend_user_id = ${userId} 
                          WHEN activity_type = 'notification' THEN friend_user_id = ${userId} 
                          WHEN activity_type = 'comment_mention' THEN user_id <> ${userId} AND (post_id IN (SELECT pt.post_id FROM post_tags AS pt WHERE pt.friend_user_id = ${userId}) OR post_id IN (SELECT cm.commentable_id FROM comments AS cm WHERE cm.friend_user_id = ${userId} OR cm.user_id = ${userId}))
                          WHEN activity_type = 'share' THEN TRUE 
                          ELSE TRUE 
                      END 
                      AND action_id NOT IN (select action_id from activity_marks where user_id = ${userId})
                      AND post_id NOT IN (SELECT post_id FROM post_notifications AS pn WHERE pn.user_id = ${userId} 
					                    AND pn.post_id = view_activities.post_id 
                                        AND (pn.start_date <= view_activities.created_at 
                                        AND CASE
                                            WHEN pn.end_date IS NULL THEN TRUE
                                            ELSE pn.end_date >= view_activities.created_at
                                        END)) 
                      AND CASE 
                            WHEN (SELECT activity_at FROM activity_mark_counts WHERE user_id = ${userId}) IS NOT NULL THEN
                               created_at > (SELECT activity_at FROM activity_mark_counts WHERE user_id = ${userId})
                            ELSE 
                              TRUE 
                          END
                      GROUP BY post_id,action_id) AS tmp`;

        con.query(sql, [], function (err, result, fields) {
            if (err) reject(err);

            //console.log("resutl => ",result);
            let data = {total: 0};
            if (result[0] !== undefined) {
                data = {total: result[0]["total"]}
            }
            resolve(data);
        });
    });
};

module.exports = {
    getCountActivitiesUnread
};