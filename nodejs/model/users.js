let mySqlConnection = require("./mysqlConnection");
let con = mySqlConnection.connection;

let getUser = userId => {
  return new Promise((resolve, reject) => {
    let sql =
        "SELECT id,username,profile_picture FROM users WHERE id = ? LIMIT 0,1";
    con.query(sql, [userId], function (err, result, fields) {
      if (err) reject(err);

      let user = {};
      if (result[0] !== undefined) {
        user = Object.assign({}, result[0]);
      }
      resolve(user);
    });
  });
};

let deleteUserToken = token => {
  return new Promise((resolve, reject) => {
    let sql = `DELETE FROM user_sessions WHERE firebase_token = ?`;
    con.query(sql, [token], function (err, result, fields) {
      if (err) reject(err);

      resolve(result);
    });
  });
};

module.exports = {
  getUser,
  deleteUserToken
};
