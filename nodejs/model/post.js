let mySqlConnection = require('./mysqlConnection');
let con = mySqlConnection.connection;

let getPostByUser = (userId, postId) => {

  return new Promise((resolve, reject) => {

    let sql = "SELECT * FROM posts WHERE user_id = ? AND id = ? LIMIT 0,1";
    con.query(sql, [userId, postId], function (err, result, fields) {

      if (err)
        reject(err);

      let cond = false;
      if (result[0] !== undefined) {
        cond = true;
      }

      resolve(cond);
    });
  });
};

module.exports = {
  getPostByUser,
};