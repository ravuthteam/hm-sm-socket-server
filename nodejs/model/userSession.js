let mySqlConnection = require("./mysqlConnection");
let con = mySqlConnection.connection;

let insert = (userId) => {
    return new Promise((resolve, reject) => {
        let sql = `INSERT 
                INTO 
                  user_sessions(user_id,created_at,updated_at) 
                  VALUES(?,UTC_TIMESTAMP(),UTC_TIMESTAMP())`;

        con.query(
            sql,
            [userId],
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });
};

let destroy = (token) => {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM user_sessions WHERE firebase_token = ?;`;

        con.query(
            sql,
            [token],
            function (err, result) {
                if (err) reject(err);
                resolve(result);
            }
        );
    });
};

let findBy = (userId) => {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM user_sessions WHERE user_id = ? LIMIT 0,1`;
        con.query(sql, [userId], function (err, result, fields) {
            if (err) reject(err);

            let data = {};
            if (result[0]) {
                data = Object.assign({}, result[0]);
            }
            resolve(data);
        });
    });
};


let insertNotExisted = async (userId) => {
    let userOnline = await findBy(userId);
    if (!userOnline.user_id)
        await insert(userId);
};

module.exports = {
    destroy,
    findBy,
    insertNotExisted,
    insert
};
